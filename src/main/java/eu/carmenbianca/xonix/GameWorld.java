package eu.carmenbianca.xonix;

import eu.carmenbianca.xonix.audio.SoundEffect;
import eu.carmenbianca.xonix.strategy.MonsterBallMoveContext;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class GameWorld extends java.util.Observable {

    public static final int SQUARE_LENGTH = 102;
    public static final int SQUARE_UNITS = 5;
    public static final int GAME_TICK_DELAY = 40;
    public static final java.awt.Color NO_COLOR = java.awt.Color.white;
    public static final java.awt.Color CAR_COLOR = java.awt.Color.red;
    public static final java.awt.Color SQUARE_COLOR = java.awt.Color.black;
    public static final java.awt.Color LINE_COLOR = java.awt.Color.red.darker().darker();
    public static final java.awt.Color PLAYER_COLOR = java.awt.Color.cyan;
    public static final java.awt.Color MONSTER_COLOR = java.awt.Color.orange;
    public static final java.awt.Color TICKET_COLOR = java.awt.Color.green;
    public static final int LEVEL_START = 1;
    public static final int CLOCK_START = (6 - LEVEL_START) * 2;
    public static final int LIVES_START = 3;
    public static final int CSCORE_START = 0;
    public static final int RSCORE_START = (40 + LEVEL_START * 10) * 100;
    public static final int TTIME_START = 6 - LEVEL_START;

    protected final FieldSquares fieldSquares;
    protected final Car car;
    private final java.util.Random random;
    protected java.util.ArrayList<MonsterBall> monsterBalls;
    protected java.util.ArrayList<TimeTicket> timeTickets;
    protected RealState realState;
    protected State proxyState;
    protected MonsterBallMoveContext moveContext;
    protected Queue<SoundEffect> soundQueue;

    private static GameWorld instance = new GameWorld();

    protected GameWorld() {
        super();
        this.random = new java.util.Random();
        this.fieldSquares = new FieldSquares();
        createMonsterballs();
        createTimeTickets();
        this.car = Car.getInstance();
        this.realState = new RealState();
        this.proxyState = new ProxyState(this.realState);
        this.moveContext = new MonsterBallMoveContext();
        this.soundQueue = new LinkedList<>();
    }

    public static GameWorld getInstance() {
        return instance;
    }

    public void createMonsterballs() {
        this.monsterBalls = new java.util.ArrayList<>();
        int number = random.nextInt(10) + 1;
        for (int i = 0; i < number; i++) {
            monsterBalls.add(new MonsterBall(new java.awt.geom.Point2D.Float(random.nextInt(SQUARE_LENGTH * SQUARE_UNITS - 30) + 15, random.nextInt(SQUARE_LENGTH * SQUARE_UNITS - 30) + 15), MONSTER_COLOR, 6, 6, random.nextInt(360), random.nextFloat() * 100 + 10));
        }
    }

    public void createTimeTickets() {
        this.timeTickets = new java.util.ArrayList<>();
        int number = random.nextInt(SQUARE_UNITS) + 1;
        for (int i = 0; i < number; i++) {
            timeTickets.add(new TimeTicket(new java.awt.geom.Point2D.Float(random.nextInt(SQUARE_LENGTH * SQUARE_UNITS - 30) + 15, random.nextInt(SQUARE_LENGTH * SQUARE_UNITS - 30) + 15), TICKET_COLOR, TTIME_START, 7, 7));
        }
    }

    public void addRandomMonsterBall() {
        monsterBalls.add(new MonsterBall(new java.awt.geom.Point2D.Float(random.nextInt(GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - 30) + 15,
                random.nextInt(GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - 30) + 15),
                GameWorld.MONSTER_COLOR,
                6,
                6,
                random.nextInt(360),
                random.nextFloat() * 100 + 10));
    }

    public void addRandomTimeTicket() {
        timeTickets.add(new TimeTicket(new java.awt.geom.Point2D.Float(random.nextInt(GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - 30) + 15,
                random.nextInt(GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - 30) + 15),
                GameWorld.TICKET_COLOR,
                7,
                7,
                GameWorld.TTIME_START));
    }

    public void update(float delta) {
        if (!realState.isGameOver()) {
            realState.addClock(-delta);
            for (MonsterBall monsterBall : monsterBalls) {
                monsterBall.changeLocation(fieldSquares, realState, delta);
                if (monsterBall.hasCollided()) {
                    soundQueue.add(SoundEffect.MONSTERBALL_COLLIDE);
                    realState.decreaseLives();
                    monsterBalls.remove(monsterBall);
                    break;
                }
            }
            car.changeLocation(fieldSquares, realState, delta);
            for (TimeTicket timeTicket : timeTickets) {
                if (timeTicket.contains(car.getLocation())) {
                    soundQueue.add(SoundEffect.TIMETICKET_CONSUME);
                    realState.setClock(realState.getClock() + timeTicket.getSeconds());
                    timeTickets.remove(timeTicket);
                    break;
                }
            }
        }
        setChanged();
        notifyObservers();
    }

    public void reset() {
        this.fieldSquares.reset();
        createMonsterballs();
        createTimeTickets();
        this.car.reset();
        this.realState.reset();
    }

    public State getState() {
        return proxyState;
    }

    public FieldSquares getFieldSquares() {
        return fieldSquares;
    }

    public ArrayList<MonsterBall> getMonsterBalls() {
        return monsterBalls;
    }

    public ArrayList<TimeTicket> getTimeTickets() {
        return timeTickets;
    }

    public MonsterBallMoveContext getMoveContext() {
        return moveContext;
    }

    public Queue<SoundEffect> getSoundQueue() {
        return soundQueue;
    }
}
