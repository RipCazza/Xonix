package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.GameController;

import java.awt.event.ActionEvent;

/**
 * Created by carmen on 25/11/16.
 */
public class QuitCommand extends Command {

    public QuitCommand() {
        super("Quit");
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        System.exit(0);
    }
}
