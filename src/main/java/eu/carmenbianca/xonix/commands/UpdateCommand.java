package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.GameWorld;

import java.awt.event.ActionEvent;

/**
 * Created by carmen on 25/11/16.
 */
public class UpdateCommand extends Command {

    GameWorld world;
    float delta;

    public UpdateCommand() {
        this.world = GameWorld.getInstance();
        this.delta = GameWorld.GAME_TICK_DELAY / 1000.0f;
    }

    @Override
    public void actionPerformed (ActionEvent evt) {
        world.update(delta);
    }
}
