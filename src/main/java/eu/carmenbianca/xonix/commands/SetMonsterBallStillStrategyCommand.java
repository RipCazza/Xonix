package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.strategy.MonsterBallStillStrategy;
import eu.carmenbianca.xonix.strategy.MonsterBallMoveContext;

/**
 * Created by carmen on 29/11/16.
 */
public class SetMonsterBallStillStrategyCommand extends SetMonsterBallStrategyCommand {


    public SetMonsterBallStillStrategyCommand(MonsterBallMoveContext context) {
        super("Still strategy", context);
    }

    @Override
    public void actionPerformed (java.awt.event.ActionEvent evt) {
        context.setStrategy(new MonsterBallStillStrategy());
    }
}
