package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.GameWorld;
import eu.carmenbianca.xonix.strategy.MonsterBallMoveContext;

/**
 * Created by carmen on 29/11/16.
 */
abstract public class SetMonsterBallStrategyCommand extends Command {

    protected MonsterBallMoveContext context;

    public SetMonsterBallStrategyCommand(String name, MonsterBallMoveContext context) {
        super(name);
        this.context = context;
    }
}
