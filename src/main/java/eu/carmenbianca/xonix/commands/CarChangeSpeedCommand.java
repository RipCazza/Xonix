package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.Car;

/**
 * Created by carmen on 24/11/16.
 */
public class CarChangeSpeedCommand extends Command {

    private int delta;

    public CarChangeSpeedCommand(int delta) {
        this.delta = delta;
    }

    @Override
    public void actionPerformed (java.awt.event.ActionEvent evt) {
        Car car = Car.getInstance();
        car.setSpeed(car.getSpeed() + delta);
    }
}
