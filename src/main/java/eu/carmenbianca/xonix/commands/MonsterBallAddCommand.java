package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.GameWorld;

/**
 * Created by stefan on 11/25/16.
 */
public class MonsterBallAddCommand extends Command {

    public MonsterBallAddCommand() {
        super("Add monsterball");
    }

    @Override
    public void actionPerformed (java.awt.event.ActionEvent evt) {
        GameWorld world = GameWorld.getInstance();
        world.addRandomMonsterBall();
    }
}
