package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.Car;

/**
 * Created by carmen on 24/11/16.
 */
public class CarHeadingCommand extends Command {
    private int heading;

    public CarHeadingCommand(int heading) {
        this.heading = heading;
    }

    @Override
    public void actionPerformed (java.awt.event.ActionEvent evt) {
        Car.getInstance().setHeading(heading);
    }
}
