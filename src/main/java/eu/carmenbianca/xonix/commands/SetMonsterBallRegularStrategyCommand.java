package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.strategy.MonsterBallMoveContext;
import eu.carmenbianca.xonix.strategy.MonsterBallRegularStrategy;

/**
 * Created by carmen on 29/11/16.
 */
public class SetMonsterBallRegularStrategyCommand extends SetMonsterBallStrategyCommand {

    public SetMonsterBallRegularStrategyCommand(MonsterBallMoveContext context) {
        super("Regular strategy", context);

    }

    @Override
    public void actionPerformed (java.awt.event.ActionEvent evt) {
        context.setStrategy(new MonsterBallRegularStrategy());
    }
}
