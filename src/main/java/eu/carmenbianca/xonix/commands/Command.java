package eu.carmenbianca.xonix.commands;

/**
 * Created by carmen on 24/11/16.
 */
public abstract class Command extends javax.swing.AbstractAction
        implements java.awt.event.ActionListener {

    protected Command() {
        super();
    }

    protected Command(String text) {
        super(text);
    }

    abstract public void actionPerformed (java.awt.event.ActionEvent evt);
}
