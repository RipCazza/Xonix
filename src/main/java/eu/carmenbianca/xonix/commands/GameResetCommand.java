package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.GameWorld;

/**
 * Created by carmen on 24/11/16.
 */
public class GameResetCommand extends Command {

    public GameResetCommand() {
    }

    @Override
    public void actionPerformed (java.awt.event.ActionEvent evt) {
        GameWorld.getInstance().reset();
    }
}
