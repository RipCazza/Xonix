package eu.carmenbianca.xonix.commands;

import eu.carmenbianca.xonix.GameWorld;

/**
 * Created by carmen on 24/11/16.
 */
public class TimeTicketAddCommand extends Command {

    public TimeTicketAddCommand() {
        super("Add timeticket");
    }

    @Override
    public void actionPerformed (java.awt.event.ActionEvent evt) {
        GameWorld world = GameWorld.getInstance();
        world.addRandomTimeTicket();
    }
}
