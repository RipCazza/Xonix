package eu.carmenbianca.xonix;

public class MonsterBall extends WorldObject implements Moveable {
    private int heading;
    private float speed;
    private boolean collideWithLine = false;

    public MonsterBall(java.awt.geom.Point2D.Float location, java.awt.Color color, int width, int height, int heading, float speed) {
        super(location, color, width, height);
        setHeading(heading);
        setSpeed(speed);
    }

    public int getHeading() {
        return heading;
    }

    private void setHeading(final int heading) {
        this.heading = heading;
    }

    public float getSpeed() {
        return speed;
    }

    public final void setSpeed(final float speed) {
        this.speed = speed;
    }

    public java.awt.geom.Point2D.Float nextLocation(float delta) {
        return GameWorld.getInstance().getMoveContext().executeStrategy(this, delta);
    }

    public void changeLocation(FieldSquares fieldSquares, RealState realState, float delta) {
        collideWithLine = false;

        java.awt.geom.Point2D.Float prev = getLocation();
        java.awt.geom.Point2D.Float next = nextLocation(delta);
        FieldSquare previousSquare = fieldSquares.elementAt((int) (prev.x / GameWorld.SQUARE_UNITS + 0.5), (int) (prev.y / GameWorld.SQUARE_UNITS + 0.5));
        FieldSquare nextSquare = fieldSquares.elementAt((int) (next.x / GameWorld.SQUARE_UNITS + 0.5), (int) (next.y / GameWorld.SQUARE_UNITS + 0.5));

        if (previousSquare.getColor() == GameWorld.LINE_COLOR || nextSquare.getColor() == GameWorld.LINE_COLOR) {
            collideWithLine = true;
        }

        if (previousSquare.getColor() != nextSquare.getColor()) {
            if (fieldSquares.elementAt((int) (prev.x / GameWorld.SQUARE_UNITS + 0.5), (int) (prev.y / GameWorld.SQUARE_UNITS + 0.5)).getColor() != fieldSquares.elementAt((int) (next.x / GameWorld.SQUARE_UNITS + 0.5), (int) (prev.y / GameWorld.SQUARE_UNITS + 0.5)).getColor()) {
                if (getHeading() < 180) {
                    setHeading(180 - getHeading());
                }
                else {
                    setHeading(540 - getHeading());
                }
            }
            if (fieldSquares.elementAt((int) (prev.x / GameWorld.SQUARE_UNITS + 0.5), (int) (prev.y / GameWorld.SQUARE_UNITS + 0.5)).getColor() != fieldSquares.elementAt((int) (prev.x / GameWorld.SQUARE_UNITS + 0.5), (int) (next.y / GameWorld.SQUARE_UNITS + 0.5)).getColor()) {
                setHeading(360 - getHeading());
            }
        }
        getLocation().setLocation(nextLocation(delta));
    }

    /**
     * Whether MonsterBall has collided with a line in the last changeLocation update.
     * @return
     */
    public boolean hasCollided() {
        return collideWithLine;
    }

    @Override
    public String toString() {
        return "";
        //return "loc=" + loc.x + "," + loc.y + " color=[" + color.getRed () + "," + color.getGreen () + "," + color.getBlue () + "]" + " heading=" + heading + " speed=" + speed + " radius=" + radius;
    }
}
