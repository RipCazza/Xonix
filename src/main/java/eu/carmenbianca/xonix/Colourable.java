package eu.carmenbianca.xonix;

import java.awt.*;

/**
 * Created by carmen on 24/11/16.
 */
public interface Colourable {
    public void setColor(Color color);
}
