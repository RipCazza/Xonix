package eu.carmenbianca.xonix.view;

import eu.carmenbianca.xonix.*;
import eu.carmenbianca.xonix.commands.*;

import java.util.Observable;

/**
 * Frame (window) of game.
 */
public class GameView extends javax.swing.JFrame implements java.util.Observer {

    protected final ScorePanel scorePanel;
    protected final MapPanel mapPanel;
    private final javax.swing.JPanel mainPanel;
    private GameWorld gameWorld;


    public GameView(MapPanel mapPanel, ScorePanel scorePanel) {
        this.gameWorld = GameWorld.getInstance();
        this.setTitle("Xonix Game");
        mainPanel = new javax.swing.JPanel();
        mainPanel.setLayout(new javax.swing.BoxLayout(mainPanel, javax.swing.BoxLayout.Y_AXIS));
        mainPanel.setBorder(new javax.swing.border.EmptyBorder(0, 30, 0, 30));
        this.scorePanel = scorePanel;
        mainPanel.add(scorePanel);
        this.mapPanel = mapPanel;
        mapPanel.setAlignmentX(CENTER_ALIGNMENT);
        mainPanel.add(mapPanel);
        this.add(mainPanel);
        this.createMenu();
        this.pack();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        this.setSize(new java.awt.Dimension(700, 700));
        this.setResizable(false);
        this.setVisible(true);

    }

    private void createMenu() {
        javax.swing.JMenuBar menuBar;
        javax.swing.JMenu commandMenu;
        menuBar = new javax.swing.JMenuBar();
        commandMenu = new javax.swing.JMenu("Command");
        commandMenu.add(new MonsterBallAddCommand());
        commandMenu.add(new TimeTicketAddCommand());
        commandMenu.add(new SetMonsterBallStillStrategyCommand(GameWorld.getInstance().getMoveContext()));
        commandMenu.add(new SetMonsterBallRegularStrategyCommand(GameWorld.getInstance().getMoveContext()));
        commandMenu.add(new QuitCommand());

        menuBar.add(commandMenu);
        this.setJMenuBar(menuBar);
    }

    @Override
    public void update(Observable observable, Object object) {
        scorePanel.update();
        mapPanel.update();
    }

    public ScorePanel getScorePanel() {
        return scorePanel;
    }

    public MapPanel getMapPanel() {
        return mapPanel;
    }
}
