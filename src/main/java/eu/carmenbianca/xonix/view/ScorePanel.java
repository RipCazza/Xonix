package eu.carmenbianca.xonix.view;

import eu.carmenbianca.xonix.GameWorld;

/**
 * Show stats of game.
 */
public class ScorePanel extends javax.swing.JPanel {
    final private javax.swing.JLabel level;
    final private javax.swing.JLabel time;
    final private javax.swing.JLabel lives;
    final private javax.swing.JLabel currentScore;
    final private javax.swing.JLabel requiredScore;

    public ScorePanel() {
        this.setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.X_AXIS));
        this.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 0, 10, 0));
        level = new javax.swing.JLabel("");
        this.add(level);
        this.add(javax.swing.Box.createHorizontalGlue());
        time = new javax.swing.JLabel("");
        this.add(time);
        this.add(javax.swing.Box.createHorizontalGlue());
        lives = new javax.swing.JLabel("");
        this.add(lives);
        this.add(javax.swing.Box.createHorizontalGlue());
        currentScore = new javax.swing.JLabel("");
        this.add(currentScore);
        this.add(javax.swing.Box.createHorizontalGlue());
        requiredScore = new javax.swing.JLabel("");
        this.add(requiredScore);
    }

    public void update() {
        this.level.setText("Current level: " + GameWorld.getInstance().getState().getLevel());
        this.time.setText("Remaining time: " + (int) GameWorld.getInstance().getState().getClock());
        this.lives.setText("Lives left: " + GameWorld.getInstance().getState().getLives());
        this.currentScore.setText("Current score: " + GameWorld.getInstance().getState().getCurrentScore());
        this.requiredScore.setText("Required score: " + GameWorld.getInstance().getState().getRequiredScore());
    }
}