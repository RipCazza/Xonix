package eu.carmenbianca.xonix.view;

import eu.carmenbianca.xonix.*;
import eu.carmenbianca.xonix.commands.*;

import javax.swing.*;

/**
 * Shows representation of game world.
 */
public class MapPanel extends javax.swing.JPanel {

    public MapPanel() {
        super();

        InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(KeyStroke.getKeyStroke("UP"), "goNorth");
        inputMap.put(KeyStroke.getKeyStroke("LEFT"), "goWest");
        inputMap.put(KeyStroke.getKeyStroke("DOWN"), "goSouth");
        inputMap.put(KeyStroke.getKeyStroke("RIGHT"), "goEast");
        inputMap.put(KeyStroke.getKeyStroke('i'), "increaseSpeed");
        inputMap.put(KeyStroke.getKeyStroke('l'), "decreaseSpeed");
        inputMap.put(KeyStroke.getKeyStroke('b'), "addRandomMonsterBall");
        inputMap.put(KeyStroke.getKeyStroke('k'), "newTimeTicket");
        inputMap.put(KeyStroke.getKeyStroke('q'), "quit");

        ActionMap actionMap = getActionMap();
        actionMap.put("goNorth", new CarHeadingCommand(90));
        actionMap.put("goWest", new CarHeadingCommand(180));
        actionMap.put("goSouth", new CarHeadingCommand(270));
        actionMap.put("goEast", new CarHeadingCommand(0));
        actionMap.put("increaseSpeed", new CarChangeSpeedCommand(5));
        actionMap.put("decreaseSpeed", new CarChangeSpeedCommand(-5));
        actionMap.put("addRandomMonsterBall", new MonsterBallAddCommand());
        actionMap.put("newTimeTicket", new TimeTicketAddCommand());
        actionMap.put("quit", new QuitCommand());
    }

    public void update() {
        this.repaint();
    }

    @Override
    public void paint(java.awt.Graphics graphics) {
        super.paint(graphics);

        for (FieldSquare[] squares : GameWorld.getInstance().getFieldSquares()) {
            for (FieldSquare fieldSquare : squares) {
                graphics.setColor(fieldSquare.getColor());
                graphics.fillRect((int) fieldSquare.getLocation().x, (int) fieldSquare.getLocation().y, fieldSquare.getWidth(), fieldSquare.getHeight());
            }
        }

        if (GameWorld.getInstance().getState().isGameOver()) {
            java.awt.Font font = new java.awt.Font("Helvetica", java.awt.Font.BOLD, 18);
            java.awt.FontMetrics metrics = graphics.getFontMetrics(font);
            graphics.setColor(java.awt.Color.RED);
            graphics.setFont(font);
            graphics.drawString("GAME OVER", (GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - metrics.stringWidth("GAME OVER")) / 2, (GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - metrics.getHeight()) / 2);
            return;
        }

        for (MonsterBall monsterBall : GameWorld.getInstance().getMonsterBalls()) {
            graphics.setColor(monsterBall.getColor());
            graphics.fillRect((int) monsterBall.getLocation().x, (int) monsterBall.getLocation().y, monsterBall.getWidth(), monsterBall.getHeight());
        }

        for (TimeTicket timeTicket : GameWorld.getInstance().getTimeTickets()) {
            graphics.setColor(timeTicket.getColor());
            graphics.fillRect((int) timeTicket.getLocation().x, (int) timeTicket.getLocation().y, timeTicket.getWidth(), timeTicket.getHeight());
        }

        graphics.setColor(GameWorld.CAR_COLOR);
        graphics.fillRect((int) Car.getInstance().getLocation().x, (int) Car.getInstance().getLocation().y, Car.getInstance().getWidth(), Car.getInstance().getHeight());
    }
}