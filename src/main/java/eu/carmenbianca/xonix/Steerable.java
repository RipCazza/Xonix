package eu.carmenbianca.xonix;

/**
 * @author stefan
 */
public interface Steerable {
    public int getHeading();
    public void setHeading(int heading);
}
