package eu.carmenbianca.xonix;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * WorldObject is an abstract object that represents any object that has a
 * physical representation in the game world. It has a position, a color and a
 * size.
 *
 * @author stefan
 */
public abstract class WorldObject {
    private java.awt.geom.Point2D.Float location;
    protected java.awt.Color color;

    private int width;
    private int height;

    public WorldObject(java.awt.geom.Point2D.Float location, java.awt.Color color, int width, int height) {
        this.location = location;
        this.color = color;
        this.width = width;
        this.height = height;
    }

    public Point2D.Float getLocation() {
        return location;
    }

    public void setLocation(Point2D.Float location) {
        this.location = location;
    }

    public Color getColor() {
        return color;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
