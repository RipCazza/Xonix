package eu.carmenbianca.xonix;

/**
 * @author stefan
 */
public interface Moveable {
    public float getSpeed();
    public void setSpeed(float speed);
    public java.awt.geom.Point2D.Float nextLocation(float delta);
    public void changeLocation(FieldSquares fieldSquares, RealState realState, float delta);
}