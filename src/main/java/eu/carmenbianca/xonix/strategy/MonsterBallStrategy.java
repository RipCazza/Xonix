package eu.carmenbianca.xonix.strategy;

import eu.carmenbianca.xonix.MonsterBall;

/**
 * Created by carmen on 29/11/16.
 */
public interface MonsterBallStrategy {

    public java.awt.geom.Point2D.Float nextLocation(MonsterBall monsterBall, float delta);
}
