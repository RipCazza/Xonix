package eu.carmenbianca.xonix.strategy;

import eu.carmenbianca.xonix.MonsterBall;

/**
 * Created by carmen on 29/11/16.
 */
public class MonsterBallMoveContext {

    MonsterBallStrategy strategy;

    public MonsterBallMoveContext() {
        // default
        this.strategy = new MonsterBallRegularStrategy();
    }

    public java.awt.geom.Point2D.Float executeStrategy(MonsterBall monsterBall, float delta) {
        return strategy.nextLocation(monsterBall, delta);
    }

    public MonsterBallStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(MonsterBallStrategy strategy) {
        this.strategy = strategy;
    }
}
