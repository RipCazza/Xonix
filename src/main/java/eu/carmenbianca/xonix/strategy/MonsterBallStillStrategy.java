package eu.carmenbianca.xonix.strategy;

import eu.carmenbianca.xonix.GameWorld;
import eu.carmenbianca.xonix.MonsterBall;

/**
 * Created by carmen on 29/11/16.
 */
public class MonsterBallStillStrategy implements MonsterBallStrategy {

    @Override
    public java.awt.geom.Point2D.Float nextLocation(MonsterBall monsterBall, float delta) {
        return monsterBall.getLocation();
    }
}
