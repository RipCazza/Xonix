package eu.carmenbianca.xonix.strategy;

import eu.carmenbianca.xonix.GameWorld;
import eu.carmenbianca.xonix.MonsterBall;

/**
 * Created by carmen on 29/11/16.
 */
public class MonsterBallRegularStrategy implements MonsterBallStrategy {

    @Override
    public java.awt.geom.Point2D.Float nextLocation(MonsterBall monsterBall, float delta) {
        double radians = Math.toRadians(monsterBall.getHeading());
        float newx = monsterBall.getLocation().x + delta * monsterBall.getSpeed() * (float) Math.cos(radians);
        if (newx < 0) {
            newx = 0;
        }
        else if (newx > GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - (GameWorld.SQUARE_UNITS - 1)) {
            newx = GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - (GameWorld.SQUARE_UNITS - 1);
        }
        float newy = monsterBall.getLocation().y - delta * monsterBall.getSpeed() * (float) Math.sin(radians);
        if (newy < 0) {
            newy = 0;
        }
        else if (newy > GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - (GameWorld.SQUARE_UNITS - 1)) {
            newy = GameWorld.SQUARE_LENGTH * GameWorld.SQUARE_UNITS - (GameWorld.SQUARE_UNITS - 1);
        }
        return new java.awt.geom.Point2D.Float(newx, newy);
    }
}
