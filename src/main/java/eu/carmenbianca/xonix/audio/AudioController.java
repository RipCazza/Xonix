package eu.carmenbianca.xonix.audio;

import eu.carmenbianca.xonix.GameWorld;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Optional;

/**
 * Created by carmen on 19/12/16.
 */
public class AudioController implements java.util.Observer {

    private static AudioController instance = new AudioController();

    private AudioController() {
        super();
    }

    public static AudioController getInstance() {
        return instance;
    }

    private void playSound(String name, boolean loop) {
        try {
            URL url = this.getClass().getClassLoader().getResource(name);
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            AudioFormat format = audioIn.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip clip = (Clip)AudioSystem.getLine(info);
            clip.open(audioIn);
            if (loop == false) {
                clip.start();
            }
            else {
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            }
        }
        catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    private void playSound(String name) {
        playSound(name, false);
    }

    @Override
    public void update(Observable observable, Object object) {
        if(!GameWorld.getInstance().getState().isGameOver()) {
            for (SoundEffect e : GameWorld.getInstance().getSoundQueue()) {
                if (e == SoundEffect.MONSTERBALL_COLLIDE) {
                    playSound("sound/Nodontdoit.wav");
                }
                else if (e == SoundEffect.TIMETICKET_CONSUME) {
                    playSound("sound/Howtopayherefor.wav");
                }
                else if (e == SoundEffect.TIME_UP) {
                    playSound("sound/WAHT.wav");
                }
                else if (e == SoundEffect.SQUARE_FILLED) {
                    playSound("sound/Yesyesyesgirl.wav");
                }
                else if (e == SoundEffect.BACKGROUND_MUSIC) {
                    playSound("sound/Longgone.wav", true);
                }
            }
        }
        else {
            for (SoundEffect e : GameWorld.getInstance().getSoundQueue()) {
                if (e == SoundEffect.GAMEOVER) {
                    playSound("sound/Nooo.wav");
                }
            }
        }
        GameWorld.getInstance().getSoundQueue().clear();
    }
}
