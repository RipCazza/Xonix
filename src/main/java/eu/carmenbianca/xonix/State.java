package eu.carmenbianca.xonix;

/**
 * Created by carmen on 29/11/16.
 */
abstract public class State {

    abstract public int getLevel();

    abstract public float getClock();

    abstract public int getLives();

    abstract public int getCurrentScore();

    abstract public int getRequiredScore();

    abstract public boolean isGameOver();
}
