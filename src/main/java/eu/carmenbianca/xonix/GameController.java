package eu.carmenbianca.xonix;

import eu.carmenbianca.xonix.audio.AudioController;
import eu.carmenbianca.xonix.commands.*;
import eu.carmenbianca.xonix.view.GameView;

import javax.swing.*;

/**
 * Created by carmen on 25/11/16.
 */
public class GameController {

    protected GameWorld world;
    protected GameView gameView;
    protected Timer timer;

    public GameController(GameWorld world, GameView gameView) {
        this.world = world;
        this.gameView = gameView;
        this.timer = new javax.swing.Timer(GameWorld.GAME_TICK_DELAY, new UpdateCommand());
        this.world.addObserver(this.gameView);
        this.world.addObserver(AudioController.getInstance());
    }

    public void play() {
        timer.start();
    }
}
