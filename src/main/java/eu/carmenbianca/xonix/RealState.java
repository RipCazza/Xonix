package eu.carmenbianca.xonix;

import eu.carmenbianca.xonix.audio.SoundEffect;

public class RealState extends State {
    private int level;
    private float clock;
    private int lives;
    private int currentScore;
    private int requiredScore;
    private int tTime;
    private boolean gameOver;

    public RealState() {
        this.reset();
    }

    public void reset() {
        setLevel(1);
    }

    @Override
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
        this.clock = (6 - level) * 2;
        this.lives = 3;
        this.currentScore = 0;
        this.requiredScore = (40 + level * 10) * 100;
        this.tTime = level - 1;
        this.gameOver = false;
    }

    @Override
    public float getClock() {
        return clock;
    }

    public void setClock(float clock) {
        this.clock = clock;
        if ((int) clock == 0) {
            decreaseLives();
            GameWorld.getInstance().getSoundQueue().add(SoundEffect.TIME_UP);
            this.clock = (6 - level) * 2;
        }
    }

    public void addClock(float clock) {
        setClock(this.clock + clock);
    }

    @Override
    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public void decreaseLives() {
        setLives(getLives() - 1);
        if (getLives() == 0) {
            GameWorld.getInstance().getSoundQueue().add(SoundEffect.GAMEOVER);
            gameOver = true;
        }
    }

    @Override
    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
        if (currentScore > requiredScore) {
            setLevel(level + 1);
        }
    }

    public void addCurrentScore(int currentScore) {
        setCurrentScore(this.currentScore + currentScore);
    }

    @Override
    public int getRequiredScore() {
        return requiredScore;
    }

    public void setRequiredScore(int requiredScore) {
        this.requiredScore = requiredScore;
    }

    @Override
    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    @Override
    public String toString() {
        return "Current level=" + this.getLevel()
                + " Remaining lives=" + this.getLives()
                + " Remaining time=" + this.getClock()
                + " Current scorePanel=" + this.getCurrentScore()
                + " Required scorePanel=" + this.getRequiredScore();
    }
}