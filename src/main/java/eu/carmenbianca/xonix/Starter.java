package eu.carmenbianca.xonix;

import eu.carmenbianca.xonix.audio.SoundEffect;
import eu.carmenbianca.xonix.view.GameView;
import eu.carmenbianca.xonix.view.MapPanel;
import eu.carmenbianca.xonix.view.ScorePanel;

public class Starter {
    public static void main(String[] args) {
        GameWorld world = GameWorld.getInstance();
        GameView view = new GameView(new MapPanel(), new ScorePanel());
        GameController controller = new GameController(world, view);
        controller.play();
        GameWorld.getInstance().getSoundQueue().add(SoundEffect.BACKGROUND_MUSIC);
    }
}
