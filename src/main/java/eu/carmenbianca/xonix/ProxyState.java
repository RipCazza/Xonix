package eu.carmenbianca.xonix;

import com.sun.org.apache.regexp.internal.RE;

/**
 * Created by carmen on 29/11/16.
 */
public class ProxyState extends State {

    private RealState realState;

    protected ProxyState(RealState realState) {
        this.realState = realState;
    }

    public int getLevel() {
        return realState.getLevel();
    }

    public float getClock() {
        return realState.getClock();
    }

    public int getLives() {
        return realState.getLives();
    }

    public int getCurrentScore() {
        return realState.getCurrentScore();
    }

    public int getRequiredScore() {
        return realState.getRequiredScore();
    }

    public boolean isGameOver() {
        return realState.isGameOver();
    }
}