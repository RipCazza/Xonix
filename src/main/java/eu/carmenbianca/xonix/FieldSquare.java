package eu.carmenbianca.xonix;

import java.awt.*;

/**
 * Square on the board.
 */
public class FieldSquare extends WorldObject implements Colourable {

    public FieldSquare(java.awt.geom.Point2D.Float location, java.awt.Color color, int width, int height) {
        super(location, color, width, height);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "";
        //return "loc=" + loc.x + "," + loc.y + " color=[" + color.getRed () + "," + color.getGreen () + "," + color.getBlue () + "]" + " size=" + size;
    }
}
